#ifndef MESH_H
#define MESH_H

#include <QVector3D>
#include <qopengl.h>
#include <QVector>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>

struct Triangle
{
    int points[3];

    int get(int i){return points[i];}

    Triangle(int a, int b, int c)
    {
        points[0] = a;
        points[1] = b;
        points[2] = c;
    }
};

class Mesh
{
public:
    Mesh();
    ~Mesh();

    void draw();

    int verticesCount(){return vertices.size();}

    int rawVerticesCount(){return rawVertices.size();}

    int trianglesCount(){return triangles.size();}

    int rawTrianglesCount(){return rawTriangles.size();}

    QVector3D* getVertices() {return vertices.data();}

    GLfloat* getRawVertices() {return rawVertices.data();}

    Triangle* getTriangles() {return triangles.data();}

    GLuint* getRawTriangles() {return rawTriangles.data();}

    void paintGL(QOpenGLShaderProgram *program);

    void initBuffer(QOpenGLFunctions* context);

private:
    QVector<QVector3D> vertices;
    QVector<GLfloat> rawVertices;

    QVector<Triangle> triangles;
    QVector<GLuint> rawTriangles;

    QOpenGLBuffer vertexBuffer = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    QOpenGLBuffer indexBuffer = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);

    void buildCube(float size);

    void setupVertices();
    void setupTriangles();
};

#endif // MESH_H
