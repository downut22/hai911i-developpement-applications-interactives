#include "mesh.h"

Mesh::Mesh()
{
    buildCube(0.1f);
    setupVertices();
    setupTriangles();
}

Mesh::~Mesh()
{

}


void Mesh::paintGL(QOpenGLShaderProgram *program)
{
    vertexBuffer.bind();
    indexBuffer.bind();

    int vertexLocation = program->attributeLocation("vertex");
    program->enableAttributeArray(vertexLocation);
    program->setAttributeBuffer(vertexLocation, GL_FLOAT, 0, 3, sizeof(GLfloat));


    glDrawElements(GL_TRIANGLES, rawTrianglesCount(), GL_UNSIGNED_INT, nullptr);
}

void Mesh::initBuffer(QOpenGLFunctions* context)
{
    vertexBuffer.create();
    vertexBuffer.bind();
    vertexBuffer.allocate(getRawVertices(), rawVerticesCount() * sizeof(GLfloat));
    //vertexBuffer.allocate(getVertices(), verticesCount() * sizeof(QVector3D));
    //glBufferData(GL_ARRAY_BUFFER,verticesCount() * sizeof(QVector3D),&vertices[0],GL_STATIC_DRAW);

    //context->glEnableVertexAttribArray(0);
    //context->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);

    indexBuffer.create();
    indexBuffer.bind();
    indexBuffer.allocate(getRawTriangles(), rawTrianglesCount() * sizeof(GLuint));
}


void Mesh::setupVertices()
{
    for(QVector3D v : vertices)
    {
        rawVertices.append(v.x());
        rawVertices.append(v.y());
        rawVertices.append(v.z());
    }
}

void Mesh::setupTriangles()
{
    for(Triangle t : triangles)
    {
        rawTriangles.append(t.get(0));
        rawTriangles.append(t.get(1));
        rawTriangles.append(t.get(2));
    }
}


void Mesh::buildCube(float size)
{

    vertices.append(QVector3D(0.1,0,0.1));
    vertices.append(QVector3D(0.1,0,-0.1));
    vertices.append(QVector3D(-0.1,0,-0.1));
    vertices.append(QVector3D(-0.1,0,0.1));

    triangles.append(Triangle(0,1,2));
    triangles.append(Triangle(1,2,3));

    /*vertices.append(QVector3D(size,size,size));
    vertices.append(QVector3D(size,size,-size));
    vertices.append(QVector3D(-size,size,-size));
    vertices.append(QVector3D(-size,size,size));

    vertices.append(QVector3D(size,-size,size));
    vertices.append(QVector3D(size,-size,-size));
    vertices.append(QVector3D(-size,-size,-size));
    vertices.append(QVector3D(-size,-size,size));*/


    //triangles.append(Triangle(0,1,2));
    //triangles.append(Triangle(2,3,0));

    //triangles.append(Triangle(4,5,6));
    //triangles.append(Triangle(6,7,4));
/*
    triangles.append(Triangle(0,1,5));
    triangles.append(Triangle(5,4,0));

    triangles.append(Triangle(1,2,6));
    triangles.append(Triangle(6,5,1));

    triangles.append(Triangle(2,3,7));
    triangles.append(Triangle(7,6,2));

    triangles.append(Triangle(3,0,4));
    triangles.append(Triangle(4,7,3));*/
}
